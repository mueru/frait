﻿using System;
using System.Linq;

namespace ConsoleApplication1
{
    internal static class Program
    {
        public static void Main(string[] args)
        {
            Multiplication_Table();
        }

        private static void Multiplication_Table()
        {
            Enumerable.Range(1,9).ToList().ForEach(x=>Enumerable.Range(1,9).Select(y=>x*y).ToList().ForEach(s=>Console.Write("{0,3}",s+(s/x%9==0?"\n":" "))));
        }

        private static void FizzBuzz()
        {
            Enumerable.Range(1,100).Select(x=>x%15==0?"FizzBuzz":x%3==0?"Fizz":x%5==0?"Buzz":x.ToString()).ToList().ForEach(Console.WriteLine);
        }
    }
}
